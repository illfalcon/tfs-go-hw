package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
	"time"
)

const companyNewsType = "company_news"
const newsType = "news"

//News is used to parse JSON file
type News struct {
	ID          int       `json:"id"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	Provider    string    `json:"provider"`
	PublishedAt time.Time `json:"published_at"`
	Tickers     []string  `json:"tickers"`
}

//NewsGroup will be used as a map key as
//strings, unlike slices, are comparable
type NewsGroup struct {
	Date    string
	Tickers string
}

//FeedNews is a struct to store an output object of type 'news'
type FeedNews struct {
	Type    string `json:"type"`
	Payload News   `json:"payload"`
}

//Payload is a struct to store necessary information in 'company_news'
type Payload struct {
	Items       []News    `json:"items"`
	PublishedAt time.Time `json:"published_at"`
	Tickers     []string  `json:"tickers"`
}

//FeedCompanyNews is a struct to store an output object of type 'company_news'
type FeedCompanyNews struct {
	Type    string  `json:"type"`
	Payload Payload `json:"payload"`
}

//Feed interface is used to store both types of output data in one slice and to sort this slice.
type Feed interface {
	FeedType() string
	PublicationDate() time.Time
}

func (n FeedNews) FeedType() string {
	return n.Type
}

func (n FeedNews) PublicationDate() time.Time {
	return n.Payload.PublishedAt
}

func (n FeedCompanyNews) FeedType() string {
	return n.Type
}

func (n FeedCompanyNews) PublicationDate() time.Time {
	return n.Payload.PublishedAt
}

func NewFeedNews(news News) *FeedNews {
	feedNews := FeedNews{Type: newsType, Payload: news}
	return &feedNews
}

func NewFeedCompanyNews(news []News) *FeedCompanyNews {
	sort.SliceStable(news, func(i, j int) bool {
		return news[i].PublishedAt.Before(news[j].PublishedAt)
	})
	payload := Payload{Items: news, PublishedAt: news[0].PublishedAt, Tickers: news[0].Tickers}
	feedCompanyNews := FeedCompanyNews{Type: companyNewsType, Payload: payload}
	return &feedCompanyNews
}

func ReadNews(path string) ([]News, error) {
	var news []News
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("unable to open a json file: %s", err)
	}
	defer file.Close()
	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("unable to read a json file: %s", err)
	}
	if err = json.Unmarshal(bytes, &news); err != nil {
		return nil, fmt.Errorf("unable to decode json file: %s", err)
	}
	return news, nil
}

//GetNewsGroup extracts news group info to use as a key in a map (key is publication date and tickers).
func (n News) GetNewsGroup() NewsGroup {
	tickers := make([]string, len(n.Tickers))
	copy(tickers, n.Tickers)
	sort.Strings(tickers)
	keyString := strings.Join(tickers, "")
	return NewsGroup{Date: n.PublishedAt.Format("2006-01-02"), Tickers: keyString}
}

//GroupNews creates a map with news grouped by publication date and tickers.
func GroupNews(news []News) map[NewsGroup][]News {
	feedMap := make(map[NewsGroup][]News)
	for _, newsObj := range news {
		group := newsObj.GetNewsGroup()
		feedMap[group] = append(feedMap[group], newsObj)
	}
	return feedMap
}

//GroupedNewsToSortedFeed creates a necessary feed structure ready to be serialized.
func GroupedNewsToSortedFeed(groupedNews map[NewsGroup][]News) []Feed {
	var feed []Feed
	for _, v := range groupedNews {
		if len(v) > 1 {
			feed = append(feed, NewFeedCompanyNews(v))
		} else {
			feed = append(feed, NewFeedNews(v[0]))
		}
	}
	sort.SliceStable(feed, func(i, j int) bool {
		return (feed[i].PublicationDate().Equal(feed[j].PublicationDate()) &&
			feed[i].FeedType() == companyNewsType && feed[j].FeedType() == newsType) ||
			feed[i].PublicationDate().Before(feed[j].PublicationDate())
	})
	return feed
}

func main() {
	var path string
	flag.StringVar(&path, "file", "", "")
	flag.Parse()
	news, err := ReadNews(path)
	if err != nil {
		log.Fatalf("error while decoding a json file: %s", err)
	}
	feedMap := GroupNews(news)
	feed := GroupedNewsToSortedFeed(feedMap)
	jsonData, err := json.MarshalIndent(feed, "", "    ")
	if err != nil {
		panic(fmt.Errorf("error while converting feed to json: %s", err))
	}
	output, err := os.Create("out.json")
	if err != nil {
		panic(fmt.Errorf("error while creating an output file: %s", err))
	}
	defer output.Close()
	_, err = output.Write(jsonData)
	if err != nil {
		panic(fmt.Errorf("error while writing to an output file: %s", err))
	}
}
