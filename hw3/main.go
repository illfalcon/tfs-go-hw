package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

type OHLC struct {
	Open  float64
	High  float64
	Low   float64
	Close float64
}

type Trade struct {
	Ticker      string
	Price       float64
	TimeOfTrade time.Time
}

const firstTrade = "2019-01-30 07:00:00.000000"

func ReadFile(file *os.File, start <-chan struct{}) (chan Trade, chan Trade, chan Trade) {
	out5 := make(chan Trade)
	out30 := make(chan Trade)
	out240 := make(chan Trade)
	go func() {
		defer close(out5)
		defer close(out30)
		defer close(out240)
		<-start
		reader := csv.NewReader(file)
		for {
			line, err := reader.Read()
			if err == io.EOF {
				break
			} else if err != nil {
				fmt.Printf("error while reading a file: %v\n", err)
				continue
			}
			var trade Trade
			trade.Ticker = line[0]
			trade.Price, err = strconv.ParseFloat(line[1], 64)
			if err != nil {
				fmt.Printf("error while parsing file: error in line %s: %v\n", line, err)
				continue
			}
			trade.TimeOfTrade, err = time.Parse("2006-01-02 15:04:05.999999", line[3])
			if err != nil {
				fmt.Printf("error while calculating candleMap: error in line %s: %v\n", line, err)
				continue
			}
			out5 <- trade
			out30 <- trade
			out240 <- trade
		}
	}()
	return out5, out30, out240
}

func sendCandles(candle map[string]*OHLC, candles chan []string, startOfPeriod time.Time) {
	for k, v := range candle {
		result := []string{k, startOfPeriod.Format(time.RFC3339),
			strconv.FormatFloat(v.Open, 'f', 2, 64),
			strconv.FormatFloat(v.High, 'f', 2, 64),
			strconv.FormatFloat(v.Low, 'f', 2, 64),
			strconv.FormatFloat(v.Close, 'f', 2, 64),
		}
		candles <- result
		delete(candle, k)
	}
}

func renewCandleMap(candleMap map[string]*OHLC, ticker string, price float64) {
	ohlc, ok := candleMap[ticker]
	if !ok {
		candleMap[ticker] = &OHLC{Open: price, High: price, Low: price, Close: price}
		return
	}
	if price > ohlc.High {
		ohlc.High = price
	} else if price < ohlc.Low {
		ohlc.Low = price
	}
	ohlc.Close = price
}

func calculateCandle(interval int, in <-chan Trade) chan []string {
	startOfPeriod, err := time.Parse("2006-01-02 15:04:05.999999", firstTrade)
	if err != nil {
		fmt.Printf("error while calculating candleMap: %v\n", err)
	}
	candleMap := make(map[string]*OHLC)
	candles := make(chan []string, 3)
	go func() {
		for trade := range in {
			endOfPeriod := startOfPeriod.Add(time.Duration(interval) * time.Minute)
			if trade.TimeOfTrade.Hour() >= 0 && trade.TimeOfTrade.Hour() < 7 {
				continue
			}
			if endOfPeriod.Sub(trade.TimeOfTrade) <= 0 {
				sendCandles(candleMap, candles, startOfPeriod)
				startOfPeriod = endOfPeriod.Add(time.Minute * time.Duration(interval) * time.Duration(int(trade.TimeOfTrade.Sub(endOfPeriod).Minutes())/interval))
			}
			renewCandleMap(candleMap, trade.Ticker, trade.Price)
		}
		sendCandles(candleMap, candles, startOfPeriod)
		close(candles)
	}()
	return candles
}

func CalculateCandles(in5, in30, in240 <-chan Trade) (out5, out30, out240 chan []string) {
	out5 = calculateCandle(5, in5)
	out30 = calculateCandle(30, in30)
	out240 = calculateCandle(240, in240)
	return
}

func writeToFile(output *os.File, in <-chan []string, wg *sync.WaitGroup) {
	writer := csv.NewWriter(output)
	for candle := range in {
		err := writer.Write(candle)
		if err != nil {
			fmt.Printf("error while writing to a file in candle %+v: %v\n", candle, err)
			continue
		}
		writer.Flush()
	}
	wg.Done()
}

func WriteToFiles(in5, in30, in240 <-chan []string, output5, output30, output240 *os.File) <-chan struct{} {
	exitToken := make(chan struct{})
	var wg sync.WaitGroup
	wg.Add(3)
	go writeToFile(output5, in5, &wg)
	go writeToFile(output30, in30, &wg)
	go writeToFile(output240, in240, &wg)
	go func() {
		wg.Wait()
		exitToken <- struct{}{}
	}()
	return exitToken
}

func main() {
	var path string
	flag.StringVar(&path, "file", "", "")
	flag.Parse()
	file, err := os.Open(path)
	if err != nil {
		log.Fatalf("error opening a file: %v", err)
	}
	defer file.Close()
	file5 := fmt.Sprintf("candles_5min.csv")
	file30 := fmt.Sprintf("candles_30min.csv")
	file240 := fmt.Sprintf("candles_240min.csv")
	output5, err := os.Create(file5)
	if err != nil {
		log.Fatalf("error creating an output file: %v", err)
	}
	defer output5.Close()
	output30, err := os.Create(file30)
	if err != nil {
		log.Fatalf("error creating an output file: %v", err)
	}
	defer output30.Close()
	output240, err := os.Create(file240)
	if err != nil {
		log.Fatalf("error creating an output file: %v", err)
	}
	defer output240.Close()
	timeout := 5 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	start := make(chan struct{})
	c5, c30, c240 := ReadFile(file, start)
	w5, w30, w240 := CalculateCandles(c5, c30, c240)
	exit := WriteToFiles(w5, w30, w240, output5, output30, output240)
	start <- struct{}{}
	for {
		select {
		case <-exit:
			return
		case <-ctx.Done():
			fmt.Println("Process timed out")
			return
		}
	}
}
